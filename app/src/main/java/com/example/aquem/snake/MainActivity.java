package com.example.aquem.snake;

import android.app.Activity;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;

import java.util.Objects;

public class MainActivity extends Activity {

    SnakeView snakeView;
    String dificultad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dificultad = Objects.requireNonNull(getIntent().getExtras()).getString("dificultad");

        //resolución de la pantalla
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        switch (dificultad) {
            case "facil":
                snakeView = new SnakeView(this, size, 5, 20);
                break;
            case "medio":
                snakeView = new SnakeView(this, size, 10, 30);
                break;
            case "dificil":
                snakeView = new SnakeView(this, size, 16, 40);
                break;
        }

        setContentView(snakeView);
    }

    @Override
    protected void onResume() {
        super.onResume();
        snakeView.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        snakeView.pause();
    }
}
