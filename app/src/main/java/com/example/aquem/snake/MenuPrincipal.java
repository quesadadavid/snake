package com.example.aquem.snake;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MenuPrincipal extends Activity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void facil(View v) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("dificultad", "facil");
        startActivity(intent);
    }

    public void medio(View v) {
        Intent intent = new Intent(MenuPrincipal.this, MainActivity.class);
        intent.putExtra("dificultad", "medio");
        startActivity(intent);
    }

    public void dificil(View v) {
        Intent intent = new Intent(MenuPrincipal.this, MainActivity.class);
        intent.putExtra("dificultad", "dificil");
        startActivity(intent);
    }

    public void ayuda(View v){
        Intent intent =  new Intent(this, Ayuda.class);
        this.startActivity(intent);
    }


}
