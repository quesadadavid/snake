package com.example.aquem.snake;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.media.SoundPool;

import java.io.IOException;

public class Sonido {
    private Context context;
    private SoundPool sp;
    private int mouse_sound;
    private int dead_sound;

    Sonido(Context context) {
        this.context = context;
        this.mouse_sound = -1;
        this.dead_sound = -1;
    }

    void loadSound() {
        sp = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
        try {
            AssetManager assetManager = context.getAssets();
            AssetFileDescriptor descriptor;

            descriptor = assetManager.openFd("get_mouse_sound.ogg");
            mouse_sound = sp.load(descriptor, 0);

            descriptor = assetManager.openFd("death_sound.ogg");
            dead_sound = sp.load(descriptor, 0);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public SoundPool getSp() {
        return sp;
    }

    public void setSp(SoundPool sp) {
        this.sp = sp;
    }

    void sonidoMuerte() {
        sp.play(dead_sound, 1, 1, 0, 0, 1);
    }

    void sonidoMouse() {
        sp.play(mouse_sound, 1, 1, 0, 0, 1);
    }
}