package com.example.aquem.snake;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.SurfaceHolder;

class Graficos {
    private int blockSize;
    private Paint paint;
    private SurfaceHolder holder;

    Graficos(int blockSize, SurfaceHolder holder) {
        this.blockSize = blockSize;
        this.holder = holder;
        this.paint = new Paint();
    }

    void drawGame(Snake snake, Mouse mouse, int puntos) {
        if (holder.getSurface().isValid()) {

            Canvas canvas = holder.lockCanvas();
            canvas.drawColor(Color.argb(255, 154, 204, 153));
            paint.setColor(Color.argb(255, 0, 0, 0));

            paint.setTextSize(50);
            canvas.drawText("Score:" + puntos, 10, 30, paint);

            for (int i = 0; i < snake.getLenght(); i++) {
                canvas.drawRect(snake.getX(i) * blockSize, (snake.getY(i) * blockSize),
                        (snake.getX(i) * blockSize) + blockSize,
                        (snake.getY(i) * blockSize) + blockSize,
                        paint);
            }

            canvas.drawRect(mouse.getX() * blockSize, (mouse.getY() * blockSize),
                    (mouse.getX() * blockSize) + blockSize,
                    (mouse.getY() * blockSize) + blockSize, paint);

            holder.unlockCanvasAndPost(canvas);
        }
    }
}
