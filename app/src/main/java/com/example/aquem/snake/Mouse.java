package com.example.aquem.snake;

import java.util.Random;

class Mouse {

    private int x;
    private int y;

    Mouse() {
        x = 0;
        y = 0;
    }

    int getX() {
        return x;
    }

    private void setX(int x) {
        this.x = x;
    }

    int getY() {
        return y;
    }

    private void setY(int y) {
        this.y = y;
    }

    void spawnMouse(int wide, int high) {
        Random random = new Random();
        setX(random.nextInt(wide - 1) + 1);
        setY(random.nextInt(high - 1) + 1);
    }
}
