package com.example.aquem.snake;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Point;
import android.view.MotionEvent;
import android.view.SurfaceView;

@SuppressLint("ViewConstructor")
class SnakeView extends SurfaceView implements Runnable {

    private Thread thread = null;
    private volatile boolean is_playing;
    private long next_frame_time;

    Graficos graficos;

    private int puntos;

    Snake snake;
    Mouse mouse = new Mouse();
    Sonido sonido;

    // Para seguir movimiento m_Direction
    public enum Direction {
        UP, RIGHT, DOWN, LEFT
    }

    private Direction direction = Direction.RIGHT;

    //Tamaño de la pantalla
    private int num_blocks_wide;
    private int num_blocks_high;
    private int screen_width;

    private int FPS;

    public SnakeView(Context context, Point size, int fps, int wide) {
        super(context);
        this.FPS = fps;
        this.num_blocks_wide = wide;
        screen_width = size.x;
        int m_ScreenHeight = size.y;
        int tam_bloque = screen_width / num_blocks_wide;
        num_blocks_high = m_ScreenHeight / tam_bloque;
        sonido = new Sonido(context);
        sonido.loadSound();
        graficos = new Graficos(tam_bloque, getHolder());
        snake = new Snake(num_blocks_wide, num_blocks_high);
        startGame();
    }

    @Override
    public void run() {
        while (is_playing) {
            if (checkForUpdate()) {
                updateGame();
                graficos.drawGame(snake, mouse, puntos);
            }
        }
    }

    public void pause() {
        is_playing = false;
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void resume() {
        is_playing = true;
        thread = new Thread(this);
        thread.start();
    }

    public void startGame() {
        snake.reset(num_blocks_wide, num_blocks_high);
        mouse.spawnMouse(num_blocks_wide, num_blocks_high);
        puntos = 0;
        next_frame_time = System.currentTimeMillis();
    }

    private void eatMouse() {
        snake.grow();
        mouse.spawnMouse(num_blocks_wide, num_blocks_high);
        puntos = puntos + 1;
        sonido.sonidoMouse();
    }

    public void updateGame() {
        if (snake.getHeadX() == mouse.getX() && snake.getHeadY() == mouse.getY()) {
            eatMouse();
        }
        snake.move(direction);

        if (snake.hasDied(num_blocks_wide, num_blocks_high)) {
            sonido.sonidoMuerte();
            startGame();
        }
    }

    public boolean checkForUpdate() {
        if (next_frame_time <= System.currentTimeMillis()) {
            long MILLIS_IN_A_SECOND = 1000;
            //long FPS = 10;
            next_frame_time = System.currentTimeMillis() + MILLIS_IN_A_SECOND / FPS;
            return true;
        }
        return false;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent motionEvent) {

        switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_UP:
                if (motionEvent.getX() >= screen_width / 2) {
                    switch (direction) {
                        case UP:
                            direction = Direction.RIGHT;
                            break;
                        case RIGHT:
                            direction = Direction.DOWN;
                            break;
                        case DOWN:
                            direction = Direction.LEFT;
                            break;
                        case LEFT:
                            direction = Direction.UP;
                            break;
                    }
                } else {
                    switch (direction) {
                        case UP:
                            direction = Direction.LEFT;
                            break;
                        case LEFT:
                            direction = Direction.DOWN;
                            break;
                        case DOWN:
                            direction = Direction.RIGHT;
                            break;
                        case RIGHT:
                            direction = Direction.UP;
                            break;
                    }
                }
        }
        return true;
    }
}