package com.example.aquem.snake;

class Snake {
    private int[] x;
    private int[] y;
    private int lenght;

    Snake(int wide, int high){
        this.x = new int[200];
        this.y = new int[200];
        this.x[0] = wide/2;
        this.y[0] = high/2;

        this.lenght = 1;
    }

    int getX(int i) {
        return x[i];
    }

    int getY(int i) {
        return y[i];
    }

    int getLenght() {
        return lenght;
    }

    int getHeadX(){
        return this.x[0];
    }

    int getHeadY(){
        return this.y[0];
    }

    void move(SnakeView.Direction direction){
        for (int i = this.lenght; i > 0; i--) {
            this.x[i] = this.x[i-1];
            this.y[i] = this.y[i-1];
        }
        switch (direction) {
            case UP:
                this.y[0]--;
                break;
            case RIGHT:
                this.x[0]++;
                break;
            case DOWN:
                this.y[0]++;
                break;
            case LEFT:
                this.x[0]--;
                break;
        }
    }

    private boolean hitWall(int wide, int high){
        boolean dead = false;

        if (this.x[0] == -1) dead = true;
        if (this.x[0] >= wide) dead = true;
        if (this.y[0] == -1) dead = true;
        if (this.y[0] == high) dead = true;

        return dead;
    }

    private boolean eatItself(){
        boolean dead = false;

        for (int i = this.lenght - 1; i > 0; i--) {
            if ((i > 4) && (this.x[0] == this.x[i]) && (this.y[0] == this.y[i])) {
                dead = true;
            }
        }

        return dead;
    }

    void reset(int wide, int high){
        this.x[0] = wide/2;
        this.y[0] = high/2;

        this.lenght = 1;

    }

    void grow() {
        this.lenght++;
    }

    boolean hasDied(int wide, int high){
        boolean dead = false;
        if (this.hitWall(wide,high))
            dead = true;
        if (this.eatItself())
            dead = true;
        return dead;
    }
}